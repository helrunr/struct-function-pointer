#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
* Define the member function prototype then define the
* function.
*/
typedef void (*print_message)(const char*);

void message(const char *msg)
{
    printf("%s\n", msg);
}

/*
* Define struct which contains function.
*/
typedef struct a_struct
{
    print_message prnt_mess;
}i_struct;

/*
* Define caller function. This is the function that will
* pull it all together.
*
* You pass it the structure containing the function and the
* value you wish to pass to that function.
*/
void caller(i_struct *funcptr, const char *call_mess)
{
    funcptr->prnt_mess(call_mess);
}


int main(int argc, char *argv[])
{
    /*
    * Variable definitions
    *
    * 1. A character array to store the message in.
    * 2. A pointer to the structure containing the function.
    */
    char sz_mess[32] = {0};
    i_struct *pi_struct = NULL;

    /*
    * Allocate memory to the pointer.
    */
    pi_struct = malloc(sizeof(i_struct));

    /*
    * Copy a message into the array.
    */
    strcpy(sz_mess, "Hello World.");

    /*
    * This assigns the function inside the struct to the
    * address of the message function.
    */
    pi_struct->prnt_mess = &message;

    /*
    * Execute the caller function passing it the pointer to the
    * structure containing the function and the message we want to print.
    */
    caller(pi_struct, sz_mess);

    /*
    * Free allocated memory.
    */
    free(pi_struct);

    return 0;

}

